import express from "express";

const app = express();

app.use(express.json());

app.get("/api/users/current-user", (req, res) => {
  res.send("Hi");
});

app.listen(3000, () => {
  console.log("Authentication service on port :3000");
});